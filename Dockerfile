FROM composer:2.0.0-alpha2
FROM php:7.4-fpm-alpine
LABEL MAINTAINER="Henny Krijnen <krijnen.h@gmail.com>"

RUN apk add --no-cache \
    bash \
    git

RUN set -xe \
	&& apk add --no-cache --virtual .build-deps \
		$PHPIZE_DEPS \
		icu-dev \
		libzip-dev \
		mysql-dev \
		postgresql-dev

RUN docker-php-ext-install \
        intl \
        zip \
        pdo \
		pdo_mysql \
		pdo_pgsql \
        iconv \
        exif \
        bcmath

ENV APCU_VERSION 5.1.18
RUN pecl install \
		apcu-${APCU_VERSION} \
	&& docker-php-ext-enable --ini-name 20-apcu.ini apcu \
	&& docker-php-ext-enable --ini-name 05-opcache.ini opcache \
	&& runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)" \
	&& apk add --no-cache --virtual .php-phpexts-rundeps $runDeps \
	&& apk del .build-deps

COPY --from=0 /usr/bin/composer /usr/bin/composer
COPY config/php.ini /usr/local/etc/php/php.ini
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint

RUN chmod +x /usr/local/bin/docker-entrypoint
ENTRYPOINT ["docker-entrypoint"]

RUN chmod 777 /usr/local/etc/php-fpm.d/www.conf

WORKDIR /srv

# only symfony addition, will be removed in the future if not used
RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

RUN symfony -V
RUN symfony php -v
RUN symfony composer -V

CMD ["php-fpm"]
