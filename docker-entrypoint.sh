#!/bin/bash
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

docker_wait() {
  START=$(date +%s)
  echo "Fichtme - PHP | Scanning $1 on port $2..."
  while ! nc -z -v $1 $2;
    do
    if [[ $(($(date +%s) - $START)) -gt ${MAX_EXECUTION_TIME:=300} ]]; then
        echo "Fichtme - PHP | Service $1 on port $2 did not start or could not be reached within ${MAX_EXECUTION_TIME:=300} seconds. Aborting..."
        exit 1
    fi
    echo "Fichtme - PHP | Retry scanning $1 on port $2 in ${SCAN_INTERVAL:=2} seconds..."
    sleep ${SCAN_INTERVAL:=2}
  done
}

PHP=`which php`

echo "Fichtme - PHP | -------------------------"
echo "Fichtme - PHP | CONTAINER_TYPE: $CONTAINER"
echo "Fichtme - PHP | HOSTNAME: $APP_URL"
echo "Fichtme - PHP | APP_ENV: $APP_ENV"
if [[ -n "$DB_HOST" ]]; then
echo "Fichtme - PHP | DB_HOST: $DB_HOST":${DB_PORT:=3306}
fi
echo "Fichtme - PHP | PHP Version: $(php -r 'echo PHP_VERSION;')"
echo "Fichtme - PHP | -------------------------"

if [[ -n "$DB_HOST" ]]; then
    docker_wait $DB_HOST ${DB_PORT:=3306}
    echo "Fichtme - PHP | DB is up"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'bin/console' ]; then
	if [ "$APP_ENV" != 'prod' ]; then
        echo "Fichtme - PHP | Development install"
		composer install --prefer-dist --no-progress --no-suggest -q
        chown -R www-data var
        chown -R www-data config
	fi

	if [[ "${AUTO_MIGRATE:0}" == 1 ]]; then
        echo "Fichtme - PHP | Applying migrations"
        php bin/console doctrine:migrations:migrate -n
    fi
fi

if [[ "${CONTAINER:-PHP}" == "MESSENGER" ]]; then
    if [[ "${AUTO_MIGRATE:0}" == 1 ]]; then
        docker_wait ${PHP_MAIN_CONTAINER_PORT_NAME:-php} ${PHP_MAIN_CONTAINER_PORT:-9000}
    fi
    while php /srv/"$@"; do
        echo "Fichtme - PHP | Rebooting"
    done
else
    echo "Fichtme - PHP | Ready"
    exec docker-php-entrypoint "$@"
fi

echo "Fichtme - PHP | Bye!"
